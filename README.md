Openwrt LEDE
============

As you probably know, because you are reading this, [LEDE](https://www.lede-project.org/)
has been announced as the reboot of the openwrt.

The main objective of this document/repo is to order everything going on, and propose a
future

Introduction
------------

I am Javier Domingo Cansino, I have worked at Fon for 3 years, later for Musaic, and now
for Jinn. My interest in OpenWRT started way before working in Fon, but it has always been
in an unreachable level. Doesn't matter how much I try, I cannot get deep enough. 
Therefore, I am speaking here as an interested user.

### Personal view

I believe in dictatorships, meritocracies and oligarchies. And I don't think everyone should
have a vote. Speaking of Open Source Projects, just in case.

I trust and believe in people.

I believe that people should be doing what they are better/happier at. Awesome developers
should be developing, the rest should assist and contribute as much as they can, be
it documentation, etc.

You do what you think you have to do for your project to improve. This includes
anything you think you should do. And this is because people believe in you,
and the ones that control the project are you.

I don't care much about if running on opensource or not, if both give the same
functionality I will choose opensource. However, I understand that there is some
people that matters that are sensible in this matter.

When I say you, I mean you as core developer.

Objective
---------

I have followed each one of the emails  in openwrt-devel and lede-adm/lede-dev. And I have
also read the IRC conversations, which have pushed me to write this.

Knowing the people that sent these emails, and the people that helped me so many times with
openwrt issues, be them user or developer level, my perception is that LEDE guys screwed
up the timing and had everyone notified at the same time, not just us.

They have stated this as such, and I have no reason to think the opposite.

The objective of the following sections is to bring together again everyone, probably doing
the same thing LEDE is doing under the name openwrt.


Problems
--------

There are several problems in the project, and I am not going to fall into who did wrong,
but an overview of the faults I have seen and talked about many times. The idea, I repeat,
of this document is to state problems and propose a future, not to blame anyone.

I have no real idea on everything, so I will mark with @@ the things I have heard
of but not experienced by myself.

  * Documentation sucks. It's maybe good for super pro developers, but I didn't manage to do
absolutely anything the 1st time, nor the 2, 3rd, etc. It needs to be improved.
  * Builds suck. There is just build testing, and they most of the times I checked fail
for failing to clone X
  * Infrastructure. @@ Must suck, and it must have a single point of failure.
  * Community. It is highly fragmentated, and you have a lot of people that depends on
the software.
  * Contributing. This is horrible. I know you all have command line email clients, but
I really prefer to make quick edits for some stuff.
  * Testing. This just doesn't exist, so I am puting it the last one.
  * Centralization. No idea, just found out that there are more core developers than I
thought

### Documentation

This is the one I can speak more about. I have read a lot of your code, and I love it.
Everyone should be using it, and know about how to use it.

I have documented some of the core libraries, and they are just awesome, but contributing
to documentation in openwrt is a pain in the ass.

Also, the fact that you use latex, docuwiki, and trac (??) doesn't help at all. The new
trend is Markdown and it's because it's legible.

There have been a lot of efforts on documenting openwrt, and they are all there, so you
have each one's heroic try, explaining the same things in different ways.

The documentation is all mixed, incomplete, outdated sometimes, with different narrative,
all sizes and colors.

### Builds

This one is just something that was done, it's been working more or less good and
covers a total need nowadays. Building your software with each commit is absolutely
required to know when you are breaking something before it's too late, and to avoid
having WIMM (Works in my machine) builds.

Also, we have the fact that they run on monstrous scripts that do a thousand things, so
basically you have the script to build openwrt and the script to build openwrt in the
builder. This really cannot happen.


### Infrastructure

@@ I have no idea, but it seems like it has downtimes.

### Community

I discovered openwrt had a community forum after 5 years of using openwrt (3 years ago).
I knew there was a openwrt-hackers list, but no idea on what it is used for. Yesterday
I discovered there is a track installation in dev.openwrt.org.

I think different people works on different ways. I personally don't like forums, but
there is people that lives in forums. Anyway, making a quick count there are probably
more than 500K posts.

The only problem I see is that there is a lot of loss of knowledge. And there is nowhere
easy to ask. Like really easy, so easy that it could serve as a reference for the next
one.

### Contributing

You can contribute in so many different ways, but at the end, you either do it as
documentation through the wiki, or as a patch through the mailing list.

The only problem is that I don't like downloading the full repo, making the changes,
committing them, signing them off, configuring git email, and reviewing how to
contribute each time I see a typo. Or I update a url because it's broken.

Take into account that nowadays the only contributors you have are the ones that
have done serious development, worth of all that process, or the ones that are
stubborn on contributing to projects when they see something wrong.

We are loosing there all the users. And I promise, users, when easy, contribute easy
things.

### Testing

This is just non-existent. It would be lovely to have, and there are so many ideas,
but no one doing them for everyone. You have your setup at home, and test what you
need.

Solutions
=========

I have talked about all the problems, and these are the solutions I propose. I
will offer my services in an email later.

Instead of structuring the solutions with the same categories, I have done it by
action to be taken.

Gitlab
------

We should use gitlab because it's easy to use for the rest of the people. If someone
wants to contribute on anything, the easier the more probable it will be.

### Features

I has a lot of features, and some of them are really interesting for us. I describe here
the possibilities, but I don't expect to use them all.

#### Triagging and labeling

In gitlab you can triage all the tickets created, so you would be able to have the code
next to the bug tracker, next to the patches. Everything could be seen at a glance.

#### Permissions

There are several levels of permissions you can have in a group.

https://gitlab.com/help/permissions/permissions

That lets you have really fine grain control on the different users, what they can do,
and would let you have people that usually gives a hand, to for example, triage
contributions, saving you time when a patch is not ready yet to be properly reviewed.

You have per group permissions and per project, being able to have a commiter in
documentation but not in the code repo.

#### Gitlab pages

You can have a webpage deployed from your repo, hosted by them directly. This way
you don't need to even have a HTTP server. I have my webpage in github pages,
http://txomon.com and a lot of companies and projects have these kind of webpages.

They are automatically build with each push, not having to care about anything else
from the first setup to the deployment.

#### TODOs and assignments

You can assign things to people, and this people can have a quick overview of their
TODOs in the project. Be it be issues, patches, etc.

We could have a base of contributors that could triage the bugs, and knowing which
those patches are for, assign them to the maintainer. That way you wouldn't need to
go around looking for work, but work would be ready for you to take care of.

Of course, depending on the level of permissions, you can reasign this to another,
or take out the assignment from you. It's an issue tracker.

#### Planifying with milestones

You can planify releases, so that you can see how much is left to acomplish a
milestone.

#### Builds / CI integration

By default, you have the chance to integrate a builder that tests your software
each time you receive a MR, or you push a commit, etc.

If using gitlab CI, you can configure what the runner should do from the code repo
through the file .gitlab-ci.yml.

You can have secrets that are ciphered, configuring the CI execution so that
each time a build succeeds, the set of packages is deployed somewhere.

There are limitless possibilities, and if you want I can set up several things
to improve the functionality.

#### Configuration features

You can configure if you want or not Issue, Merge requests, Builds, Wiki, Snippet.

You can make templates for issues, and choose merge policy, like normal merging,
merge with commit but just when branch rebased, fast-forward merges with no
merge commit.

You can make approval system, so that to merge something, people needs to aprove
it.

#### Other features

I am just going to list other features here:

  * Snippets. Instead of having a pastebin, you can have a snippet place.
  * Wiki. If you want, you have a wiki repo together to the code repo that can serve as a wiki
browsable for the users, but I don't recomend this.
  * Gitlab plugins, there are a lot of plugins (maybe not as many as github) that
use their API, and we would be able to create some for us. Google has done this with
github for example for kubernetes (CLA checker, external build bots, etc.)
  * Hosted by gitlab for free in gitlab.com!


### Requirements / Complaints

I have seen there are several requirements on this matter, so these are the answers.

#### The git is important, we should be in control

Yes, git is important, but having a mirror we can turn into master if something goes
wrong is enough. Also, if we had funding with resources assigned, we would be able
to have someone in charge of that

#### Reviewing patches in gitlab is not nice (complex patches etc.)

My idea is to maintain the current workflow and accept patches through gitlab. I
must admit it has some drawbacks, and you might be accustomed to your workflow, but 
with the feature of triagging/labeling, we could actually have someone answering questions
and the CI system testing them automatically, and have you reviewed them once
all tests are passed.

And if you have a patch that you would really want to be sent by email, you can always ask
for it.


Miscellaneous with personal opinions
------------------------------------

In first instance, for many of us it didn't make sense because it was proposed by the ones
we understood were in charge of the project, but seems like there were internal
disagreements that weren't fixed.

Also, I try to learn about everything related to ways to make developers be more
productive and happy. I read the blogs from dropbox, netflix, spotify, google, etc.
All those youngster hype things.
